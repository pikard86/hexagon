﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{
    private Renderer rend;
    private bool dragging;
    private float x;
    private float y;
    void Start()
    {
        dragging = false;
        rend = GetComponent<Renderer>();
    }
    void OnMouseOver()
    {
        //  rend.material.color -= new Color(0.1F, 0, 0) * Time.deltaTime;
    }
    void OnMouseExit()
    {
        rend.material.color = Color.white;
    }

    private void OnMouseDown()
    {
        //FIXME : Store position in order to avoid jump on movw
        Debug.Log("Drag start!");
        dragging = true;
    }
    void OnMouseEnter()
    {
        rend.material.color = Color.red;
    }
    void OnMouseUp()
    {
        Debug.Log("Drag ended!");
        dragging = false;
    }

    private void OnMouseDrag()
    {
        Debug.Log("Drag Start!");
        float z =  transform.position.z;
        transform.position = Camera.main.ScreenToWorldPoint(new Vector3(x, y, 10.0f));
        transform.position = new Vector3(transform.position.x,transform.position.y,z);
    }

    void Update()
    {
        if (dragging)
        {
            x = Input.mousePosition.x;
            y = Input.mousePosition.y;
        }
    }
}